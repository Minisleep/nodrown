Minetest mod to disable drowning, so you can swim and live underwater forever.  

Air/breathing limits are a timer that prevents thinking, planning and generally having fun.  Underwater effectively becomes a desert that people avoid where possible.  Water should be interesting, not gameplay mud.

Minetest already does underwater a lot better than Minecraft traditionally has.  You're not blind and mining speed is not reduced.  I think underwater would be more interesting if so many people did not grow up with Minecraft teaching them that it's punishment.

## Compatibility

Tested and working on:

* minetest_game
* mineclone2

Please report if you find any incompatibilities, I'll try to fix them.
